package parqueadero;

import java.util.Date;
import java.util.Iterator;
import java.util.Stack;

public class Central 
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	private Stack<Parqueadero> paqueaderoEsperando;
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	private Stack<Parqueadero> parqueadero1;
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Central ()
    {
    	int s=2;
    	parqueadero1=new Parqueadero(4,1);
    	Stack<Parqueadero> q=parqueadero1;
    	while (s<=8)
    	{
    		Parqueadero w=new Parqueadero(4,s);
    		((Parqueadero) q).cambiarSiguiente(w);
    		q=((Parqueadero) q).darSiguiente();
    		s++;
    	}
    	paqueaderoEsperando=new Parqueadero(100000,0);
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	 Stack<Carro> espera=new Carro( pColor,  pMatricula,  pNombreConductor);
    	((Parqueadero) paqueaderoEsperando).cambiarUltimoCarro(espera);
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	Carro actual=(Carro) ((Parqueadero) paqueaderoEsperando).sacarUltimoCarro();
    	String m=parquearCarro(actual);
        return "El carro con la placa "+actual.darMatricula()+" fue parqueado en el parqueadero "+m;
    } 
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {
    	Carro w=(Carro) sacarCarro(matricula);
    	if(w==null)
    	{
    		throw new Exception ("no se encontro el carro");
    	}
    	return cobrarTarifa(w);
    }
    
  /**
   * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro
   * @throws Exception
   */
    public String parquearCarro( Stack<Carro> aParquear) throws Exception
    { 
    Parqueadero w=(Parqueadero) parqueadero1;
	boolean s=false;
	int n=0;
	while(s==false&&w!=null)
	{
		n++;
		if (w.estaLLeno()==false)
		{
			w.cambiarUltimoCarro(aParquear);
			s=true;
		}
		w=w.darSiguiente();
	}
	if(s==false)
	{
		throw new Exception("no se pudo parquear");
	}
    	
    	return "PARQUEADERO "+n;    	    
    }
    
    /**
     * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public  Stack<Carro> sacarCarro (String matricula)
    {
    	Parqueadero w2=(Parqueadero) parqueadero1;
    	Carro paraSacar=null;
    	paraSacar=(Carro) w2.buscarCarro(matricula);
    	while(w2!=null&&paraSacar==null)
    	{
    		Carro s=(Carro) w2.buscarCarro(matricula);
    		if(s!=null)
    		{
    			paraSacar=s;
    			w2.sacarCarro(s);
    		}
    		w2=w2.darSiguiente();
    	}
    	return paraSacar;
    }
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa ( Stack<Carro> car)
    {
    	Date s=new Date();
    	long w= s.getTime()-((Carro) car).darLlegada();
    	double t=w/36000;
    	double t2=t*25;
    	return 	t2;
    }
    public String estadoParqueaderos()
    {
    	Parqueadero p1=(Parqueadero) parqueadero1;
    	int numero=0;
    	String s="";
    	while(p1!=null)
    	{
    		numero++;
    		String w3="";
    		for(int i=0;i<p1.totalDeCarros();i++)
    		{
    			w3=w3+" +";
    		}
    		s=s+"PARQUEADERO "+numero+": "+w3+";";
    		p1=p1.darSiguiente();
    	}
    	return s;
    }
     public String estadoPorParquear()
     {
    	 String w3="";
 		for(int i=0;i<((Parqueadero) paqueaderoEsperando).totalDeCarros();i++)
 		{
 			w3=w3+" +";
 		}
 		return "Esperando por parquear:"+w3;
     }
    
}
