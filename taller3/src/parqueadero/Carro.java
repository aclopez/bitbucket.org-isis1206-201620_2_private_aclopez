package parqueadero;


import java.util.Date;
import java.util.Stack;

public class Carro extends Stack
{
   /**
    * Color del vehiculo
    */
   private String color;
   /**
    * Matricula del vehiculo
    */
   private String matricula;
   /**
    * nombre del due�o del vehiculo
    */
   private String nombreConductor;
   /**
    * Registro ingreso del vehiculos
    */
   private Date diaHora = new Date ();
   /**
    * Registro ingreso en milisegundos
    */
   private long diaHoraMillis;
   private int numeroParqueadero;
   private Stack<Carro> siguiente;
   private int numeroEnFila;
   /**
    * Crea el registro de un nuevo vehiculo al llegar al parqueadero
    * @param pColor color del vehiculo
    * @param pMatricula matricula del vehiculo
    * @param pNombreConductor nombre de quien conduce el vehiculos
    */
   
   public Carro (String pColor, String pMatricula, String pNombreConductor) 
  {
	 color=pColor;
	 matricula=pMatricula;
	 nombreConductor= pNombreConductor;
	 diaHoraMillis=diaHora.getTime();
	 siguiente=null;
	 numeroParqueadero=-1;
	 numeroEnFila=-1;
   }
   public void cambiarNumeroEnFila(int numero)
   {
	   numeroEnFila=numero;
   }
   public int darNumeroEnFila()
   {
	   return numeroEnFila;
   }
   
   /**
    * Da el color del vehiculo
    * @return el color del vehiculo
    */
   public String darColor()
   {
	   return color;
   }
   /**
    * Da la matricula del vehiculo
    * @return la matricula del vehiculo
    */
   public String darMatricula()
   {
	   return matricula;
   }
   /**
    * Da el nombre de quien conduce el vehiculo
    * @return el nombre de quien conduce el vehiculo
    */
   public String darNombreConductor ()
   {
	   return nombreConductor;
   }
   /**
    * Devuelve en milisegundos la hora de llegada del vehiculo en el momento de su registro
    * @return la hora de registro del vehiculo en milisegundos
    */
   public long darLlegada()
   {
	   return diaHoraMillis;
   }
   public void cambiarParqueadero(int numero)
   {
	   numeroParqueadero=numero;
   }
   public int darParqueadero()
   {
	   return numeroParqueadero;
   }
   public void cambiarSiguiente (Stack<Carro> sig)
   {
	   siguiente=sig;
   }
   public Stack<Carro> darSiguiente()
   {
	   return siguiente;
   }
}
