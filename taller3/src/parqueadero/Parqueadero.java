package parqueadero;

import java.util.ArrayList;
import java.util.Stack;

public class Parqueadero <T> extends  Stack
{
	private int tama�o;
	private boolean lleno;
	private  Stack<Carro>  ultimoCarro;
	private int numero;
	private Stack<Parqueadero> siguiente;
	public Parqueadero(int pTama�o,int pNumero)
	{
		tama�o=pTama�o;
		lleno=false;
		ultimoCarro=null;
		numero=pNumero;
		siguiente=null;
	}
	public Object buscarCarro(String matricula)
	{
		Stack<Carro> buscado=null;
		 Stack<Carro> actual= (Carro)ultimoCarro;
		while(buscado==null&&actual!=null)
		{
			if(((Carro) actual).darMatricula().equals(matricula))
			{
				buscado=actual;
			}
			actual=((Carro) actual).darSiguiente();
		}
		return buscado;
	}
	public Parqueadero darSiguiente()
	{
		return (Parqueadero) siguiente;
	}
	public void cambiarSiguiente(Stack<Parqueadero> s)
	{
		siguiente=s;
	}
	public int darNumero()
	{
		return numero;
	}
	public int darTama�o()
	{
		return tama�o;
	}
	public void cambiarUltimoCarro( Stack<Carro> nuevo)
	{
		if(numero==0)
		{
			((Carro) nuevo).cambiarParqueadero(numero);
			if(ultimoCarro==null)
			{
				ultimoCarro=nuevo;
				((Carro) ultimoCarro).cambiarNumeroEnFila(1);
			}
			else
			{
				Stack<Carro> actual=ultimoCarro;
				Stack<Carro> actual1=((Carro) ultimoCarro).darSiguiente();
			    while(actual1!=null)
			    {
			    	actual=actual1;
			    	actual1=((Carro) actual1).darSiguiente();
			    }
			    ((Carro) actual).cambiarSiguiente(nuevo);
			    reorganizarParqueadero();
			}
		}
		else
		{
		    ((Carro) nuevo).cambiarParqueadero(numero);;
			((Carro) nuevo).cambiarSiguiente(ultimoCarro);
			ultimoCarro=nuevo;
			reorganizarParqueadero();
		}
	}
	public boolean estaLLeno()
	{
		int t=0;
		Carro actual=(Carro) ultimoCarro;
		while(actual!=null)
		{
			t++;
			actual=(Carro) actual.darSiguiente();
		}
		if(t==tama�o)
		{
			lleno=true;
		}
		return lleno;
	}
	public Stack<Carro> darUltimoCarro()
	{
		return (Carro) ultimoCarro;
	}
	public void eliminarUltimoCarro()
	{
		Carro actual=(Carro) ((Carro) ultimoCarro).darSiguiente();
		ultimoCarro=actual;
		reorganizarParqueadero();
	}
	public Stack<Carro> sacarUltimoCarro()
	{
		Carro s=(Carro) ultimoCarro;
		eliminarUltimoCarro();
		return s;
	}
	public void sacarCarro( Stack<Carro> s)
	{
		Carro t=(Carro) ultimoCarro;
		if(t.equals(s))
		{
			sacarUltimoCarro();
		}
		else 
			{
			Carro anterior=(Carro) ((Carro) ultimoCarro).darSiguiente();
		    boolean encontro=false;
		    while (anterior!=null&&encontro==false)
		    {
	     	  if(anterior.equals(s))
			{
				encontro=true;
				t.cambiarSiguiente(anterior.darSiguiente());
				reorganizarParqueadero();
			}
	     	t=(Carro) t.darSiguiente();
	     	anterior=(Carro) anterior.darSiguiente();
			
		}
			}
	}
	public void reorganizarParqueadero()
	{
		Carro actual2=(Carro) ultimoCarro;
		int s1=1;
		while(actual2!=null)
		{
			actual2.cambiarNumeroEnFila(s1);
			s1++;
			actual2=(Carro) actual2.darSiguiente();
		}
	}
	public int totalDeCarros()
	{
		int t=0;
		Carro actual=(Carro) ultimoCarro;
		while(actual!=null)
		{
			t++;
			actual=(Carro) actual.darSiguiente();
		}
		return t;
	}
}
